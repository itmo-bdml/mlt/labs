## ITMO university. 1st semester
## Machine Learning Technologies

---

Lecturer: Petr Gladilin

Performed by: Gleb Mikloshevich

Group: J4132C

---
Labs:
1. Supervised learning. Train Decision trees and Random forests with different parameters for binary classification task. 
2. Natural Language Processing.
3. Optimization methods. Implement and test GD with momentum and Adam optims.
4. MLP and Backpropagation. train a sigmoide function with three different optimizers: gradient descent (GD), GD with momentum, and Adam.
5. CNN and Transfer Learning: Cats vs Dogs. lab5_1.ipynb includes 1st and 2nd tasks. lab5_2.ipynb has ViT fine tuning.

     **Accuracy: 0.9884**